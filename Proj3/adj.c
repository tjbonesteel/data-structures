#include <stdio.h>
#include <stdlib.h>
#include <math.h>




struct adjListNode{
  int dest;
  struct adjListNode *next;
};

struct adjList{
  struct adjListNode *head;
};

struct graph{
  int v;
  struct adjList *array;
};

void printGraph(struct graph *graph);
void addEdge(struct graph *graph, int src, int dest);





struct adjListNode *newAdjListNode(int dest){
  struct adjListNode *newNode = 
    (struct adjListNode*)malloc(sizeof(struct adjListNode));
  newNode->dest = dest;
  newNode->next = NULL;

  return newNode;
}

struct graph *createGraph(int v){
  struct graph *graph = (struct graph*)malloc(sizeof(struct graph));
  graph->v = v;
  
  graph->array = (struct adjList*)malloc(v * sizeof(struct adjList));

  int i;
  for( i = 0;i < v; ++i){
    graph->array[i].head = NULL;
  }

  return graph;
}

void addEdge(struct graph *graph, int src, int dest){
  struct adjListNode* newNode = newAdjListNode(dest);
  newNode->next = graph->array[src].head;
  graph->array[src].head = newNode;

  newNode = newAdjListNode(src);
  newNode->next = graph->array[dest].head;
  graph->array[dest].head = newNode;
}

void printGraph(struct graph *graph)
{
  int v;
  for(v = 0; v < graph->v; ++v){
    struct adjListNode *pCrawl = graph->array[v].head;
    printf("\n Adjacency list of vertex %d\n head ",v);
    while (pCrawl){
      printf("-> %d", pCrawl->dest);
      pCrawl = pCrawl->next;
    }
    printf("\n");
  }
}


int main(int argc, char **argv)
{
  FILE *fin;
  int numVertex = 0, numEdge = 0;
  int i, id, x, y, src, dest;
  if(argc != 2){
    fprintf(stderr, "Invalid number of input arguments!\n");
    return EXIT_FAILURE;
  }
  
  fin = fopen(argv[1],"r");
  
  if(fin == NULL){
    fprintf(stderr, "Could not open the input file!\n");
    return EXIT_FAILURE;
  }
  rewind(fin);

  fscanf(fin,"%d %d",&numVertex,&numEdge);
  
  struct graph *graph = createGraph(numVertex);
  for(i = 0; i < numVertex; i++){
    fscanf(fin,"%d %d %d",&id,&x,&y);
    
  }
  for(i = 0; i < numEdge; i++){
    fscanf(fin, "%d %d",&src,&dest);
    addEdge(graph,src,dest);

  }
  printGraph(graph);
  fclose(fin);
  return EXIT_SUCCESS;
}

				       
