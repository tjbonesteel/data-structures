#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <limits.h>

#include "proj3.h"



int main(int argc, char **argv)
{
  //Local variable declarations
  FILE *fin,*fquery;
  int numVertex = 0, numEdge = 0, numQuery = 0;
  int i = 0;
  int x,y,id;
  int src, dest,qSrc,qDest;

  //Check to make sure valid map and query file given
  if(argc != 3){
    fprintf(stderr, "Invalid number of input arguments!\n");
    return EXIT_FAILURE;
  }
  

  //Open map file
  fin = fopen(argv[1],"r");
  if(fin == NULL){
    fprintf(stderr, "Could not open the map file!\n");
    return EXIT_FAILURE;
  }
  fquery = fopen(argv[2],"r");
  if(fquery == NULL){
    fprintf(stderr, "Could not open the query file!\n");
    return EXIT_FAILURE;
  }
  rewind(fquery);
  rewind(fin);
  fscanf(fin,"%d %d",&numVertex,&numEdge);
  
  graph *g  = malloc(sizeof(vertex) * numVertex);
  g->V = numVertex;

  for(i = 0; i < numVertex; i++){
    fscanf(fin,"%d %d %d",&id,&x,&y);
    
    vertex *newVertex = (vertex*)malloc(sizeof(vertex));
    newVertex->id = id;
    newVertex->x_cord = x;
    newVertex->y_cord = y;
    newVertex->next = NULL;
    newVertex->visited = 0;
    g->vertices = insert(g->vertices,newVertex);

  }
  

  for(i = 0; i < numEdge; i++){
    fscanf(fin, "%d %d",&src, &dest);
    Add_Edge(getNth(g->vertices,src), getNth(g->vertices,dest));
    }
  //print1(g);

  //Read in query data
  fscanf(fquery,"%d",&numQuery);

  for(i = 0; i < numQuery; i++){
    fscanf(fquery,"%d %d",&qSrc,&qDest);

    dijkstra(g,qSrc,qDest);

  }

  fclose(fin);
  fclose(fquery);
  return EXIT_SUCCESS;
}

//Finds the nth vertex in linked list
//Used like referencing an array index
vertex *getNth(vertex *head, int n)
{
  vertex *current = head;
  int count = 0;

  while (current != NULL)
    {
      if (count == n)
	return current;
      count++;
      current = current->next;
    }

  return current;
}

//IN ORDER INSERTION INTO LINKED LIST FOR VERTICES
vertex *insert(vertex *head, vertex *v)
{
  if(head == NULL){
    return v;
  }
  if(v->id < head->id){
    v->next = head;
    return v;
  }
  vertex *temp,*prev; 
  temp = head->next;
  prev = head;
  while(temp != NULL && temp->id <= v->id){
    prev = temp;
    temp = temp->next;
  }
  
  v->next = temp;
  prev->next = v;

  return head;
  
}

void print1(graph *g)
{
  vertex *v = g->vertices;
  
  while(v != NULL)
    {
      printf("%d:",v->id);
      print2(v);
      v = v->next;
    }

}

void print2(vertex *v)
{

  edge *e = v->adjList;
  while(e != NULL){
    printf(" -> %d(%d) ",e->connection->id, e->distance);
    e = e->next;
  }
  printf("\n");
}


//This inserts in the edges into adjacency linked list
//Standard linked list insertion
//Based off asceding distance
edge *insertEdge(edge *head, edge *e)
{
  if(head == NULL){
    return e;
  }
  if(e->distance < head->distance){
    e->next = head;
    return e;
  }
  edge *temp,*prev;
  temp = head->next;
  prev = head;
  while(temp != NULL && temp->distance > e->distance){
    prev = temp;
    temp = temp->next;
  }
  
  e->next = temp;
  prev->next = e;

  return head;
}

//Adds an edge to the graph
void Add_Edge(vertex *a, vertex *b)
{
  //Each connection is actually 2 edges
  edge *newEdge1 = (edge*)malloc(sizeof(edge*));
  edge *newEdge2 = (edge*)malloc(sizeof(edge*));
  
  int dist = Get_Distance(a, b);

  //Edge from a to b
  newEdge1->distance = dist;
  newEdge1->next = NULL;
  newEdge1->connection = b;
  a->adjList = insertEdge(a->adjList,newEdge1);
  
  //Edge from b to a
  newEdge2->distance = dist;
  newEdge2->next = NULL;
  newEdge2->connection = a;
  b->adjList = insertEdge(b->adjList,newEdge2);

}

//Get Euclidean distance between vertex a and b
int Get_Distance(vertex *a, vertex *b)
{
  int distance = 0, xDif = 0, yDif = 0;
  xDif = (b->x_cord) - (a->x_cord);
  yDif = (b->y_cord) - (a->y_cord);

  distance = sqrt(pow((xDif),2) +  pow(yDif,2));

  return distance;
}

//Standard heapify for min heap
void minHeapify(minHeap* minHeap, int idx)
{
    int smallest, left, right;
    smallest = idx;
    left = 2 * idx + 1;
    right = 2 * idx + 2;
 
    if (left < minHeap->size &&
        minHeap->array[left]->dist < minHeap->array[smallest]->dist )
      smallest = left;
 
    if (right < minHeap->size &&
        minHeap->array[right]->dist < minHeap->array[smallest]->dist )
      smallest = right;
 
    if (smallest != idx)
    {
        minHeapNode *smallestNode = minHeap->array[smallest];
        minHeapNode *idxNode = minHeap->array[idx];
 
        minHeap->pos[smallestNode->v] = idx;
        minHeap->pos[idxNode->v] = smallest;
 
        swapMinHeapNode(&minHeap->array[smallest], &minHeap->array[idx]);
 
        minHeapify(minHeap, smallest);
    }
}

// Swaps two min heap nodes
void swapMinHeapNode(minHeapNode** a, minHeapNode** b)
{
    minHeapNode* temp = *a;
    *a = *b;
    *b = temp;
}

void decreaseKey(minHeap* minHeap, int v, int dist)
{
    
    int i = minHeap->pos[v];

    minHeap->array[i]->dist = dist; 
 
    
    while (i && minHeap->array[i]->dist < minHeap->array[(i - 1) / 2]->dist)
    {
      //Keep swapping with parent
        minHeap->pos[minHeap->array[i]->v] = (i-1)/2;
        minHeap->pos[minHeap->array[(i-1)/2]->v] = i;
        swapMinHeapNode(&minHeap->array[i],  &minHeap->array[(i - 1) / 2]);
 
        i = (i - 1) / 2;
    }
}

// Standard min heap extraction
minHeapNode* extractMin(minHeap* minHeap)
{
    if (isEmpty(minHeap))
        return NULL;
 
    minHeapNode* root = minHeap->array[0];
 
    minHeapNode* lastNode = minHeap->array[minHeap->size - 1];
    minHeap->array[0] = lastNode;
 
    minHeap->pos[root->v] = minHeap->size-1;
    minHeap->pos[lastNode->v] = 0;
 
    --minHeap->size;
    minHeapify(minHeap, 0);
 
    return root;
}

// initializes the min heap 
minHeap* createMinHeap(int capacity)
{
    minHeap* minHeap = malloc(sizeof(minHeap));
    minHeap->pos = (int *)malloc(capacity * sizeof(int));
    minHeap->size = 0;
    minHeap->capacity = capacity;
    minHeap->array = (minHeapNode**) malloc(capacity * sizeof(minHeapNode*));
    return minHeap;
}

//Simple check if vertex is in heap
int isInMinHeap(minHeap *minHeap, int v)
{
   if (minHeap->pos[v] < minHeap->size)
     return 1;
   return 0;
}

//is Heap empty
int isEmpty(minHeap* minHeap)
{
    return minHeap->size == 0;
}

// initializes min heap node
minHeapNode* newMinHeapNode(int v, int dist)
{
    minHeapNode* minHeapNode = malloc(sizeof(minHeapNode));
    minHeapNode->v = v;
    minHeapNode->dist = dist;
    return minHeapNode;
}

//Carrys out the calculation of distances.
//Check all distances to all nodes
void dijkstra(graph* g, int src,int dest)
{
    int V = g->V;//number of vertices in graph
    path* dist = malloc(V * sizeof(path*)); //allocate memory for distances/pred
    int v;
    minHeap* minHeap = createMinHeap(V);
    //Put all vertices in min heap
    for (v = 0; v < V; ++v)
    {
        dist[v].dist = INT_MAX;
        minHeap->array[v] = newMinHeapNode(v, dist[v].dist);
        minHeap->pos[v] = v;
    }
 
    //set src distance to zero
    minHeap->array[src] = newMinHeapNode(src, dist[src].dist);
    minHeap->pos[src]   = src;
    dist[src].dist = 0;
    decreaseKey(minHeap, src, dist[src].dist);
 

    minHeap->size = V;
 
    while (!isEmpty(minHeap))
    {

        minHeapNode* minHeapNode = extractMin(minHeap);
	int u = minHeapNode->v;
	vertex* p = getNth(g->vertices,u);
	edge * pCrawl = p->adjList;

	while (pCrawl != NULL)
        {
            int v = pCrawl->connection->id;
            if (isInMinHeap(minHeap, v) && dist[u].dist != INT_MAX && pCrawl->distance + dist[u].dist < dist[v].dist)
            {
                dist[v].dist = dist[u].dist + pCrawl->distance;
		dist[v].pred = u;
                decreaseKey(minHeap, v, dist[v].dist);
            }
            pCrawl = pCrawl->next;
        }
    }
    
    printf("%d\n",dist[dest].dist);
    
    while(dist[dest].pred > 0){ 
      printf("%d ",dest);
      dest = dist[dest].pred;
      }
    
    printf("%d\n",src);
}

