//typedef struct Vertex Vertex;
//typedef struct Edge Edge;
//typedef struct Graph Graph;
typedef struct p_node path;
typedef struct v_node{
  unsigned int x_cord;
  unsigned int y_cord;
  int id;
  int dist;
  struct v_node *predecessor;
  struct v_node *next;
  struct e_node *adjList;
  int visited;
}vertex;


typedef struct p_node{
  int dist;
  int pred;
}path;

typedef struct e_node{
  struct v_node *connection;
  struct e_node *next;
  int distance;
}edge;

typedef struct g{
  vertex *vertices;
  int V;
}graph;

typedef struct mhn{
  int v;
  int dist;
}minHeapNode;

typedef struct mh{
    int size;
  int capacity;
  int *pos;
  minHeapNode **array;
}minHeap;

typedef struct v_node *v_heap;

int Parse_Nodes(FILE *fin);
int Parse_Adj(FILE *fin);
void Add_Edge(vertex *a, vertex *b);
struct vertex* New_Vertex(int id);
int Get_Distance(vertex *a, vertex *b);
vertex *insert(vertex *g, vertex *v);
void print1(graph *g);
void print2(vertex *v);
vertex *getNth(vertex *head, int n);
void dijkstra(graph *graph,int src,int dest);
void minHeapify(minHeap *minHeap, int idx);
int isInMinHeap(minHeap *minHeap, int v);
int isEmpty(minHeap *minHeap);
void swapMinHeapNode(minHeapNode **a, minHeapNode **b);
minHeapNode *newMinHeapNode(int v, int dist);
void decreaseKey(minHeap* minHeap, int v, int dist);
minHeapNode* extractMin(minHeap* minHeap);
void print(path *dist,int dest);
