#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "huff.h"
#define ASCII_COUNT 256

struct _fnode pool[256] = {{0}};
fnode *qqq[255], **q=qqq-1;
int n_nodes = 0, qend = 1, num_char = 1;
char *code[129] = {0}, buf[1024];
int byte[8] = {0};
unsigned char buffer;
int curBit = 0;

int main(int argc, char **argv)
{
  //local variable declaration
  FILE *fin,*fout;
  int endFlag = 0;
  char *inFilename,*outFilename;
  //Check if 2 input arguments. 
  //1. Program name
  //2. Input file name
  if (argc != 2){
    fprintf(stderr, "Incorrect number of arguements!\n");
    return EXIT_FAILURE;
  }
  inFilename = argv[1];
  //Open input file
  fin = fopen(inFilename,"r");
  if (fin == NULL){
    fprintf(stderr, "Input file could not be opened!\n");
    return EXIT_FAILURE;
  }

  Find_Freq(fin);
  outFilename = strcat(inFilename,".huff");
  fout = fopen(outFilename, "wb");

  if (fout == NULL){
    fprintf(stderr,"Output file could not be opened for writing!\n");
    return EXIT_FAILURE;
  }
  Traverse(q[1],fout);
  rewind(fin);
  Flush_Bits(fout);
  fwrite(&num_char,sizeof(int),1,fout);
  while(endFlag != -1){
    endFlag = Generate_Bitstream(fin,fout);
  }
  Flush_Bits(fout);
  fclose(fin);
  fclose(fout);
  
  return EXIT_SUCCESS;
}

//Creates a new node for a character to be added
//to the huffman tree.
fnode *New_Node(int freq, char c, fnode *a, fnode *b)
{
  fnode *n = pool + n_nodes++;
  if (freq){
    n->c = c;
    n->freq = freq;
  }
  else{
    n->left = a;
    n->right = b;
    n->freq = a->freq + b->freq;
  }
  return n;
}

//Before entering this function the file pointer
//is reset to the beginning of the file. This
//funtion simply grabs character by character
//from the input file and relates it to the
//correpsonding bit sequence in code[]. It then
//breaks down the code into individual bits and feeds
//them to the Write_Bit function to be written to
//the output file
int Generate_Bitstream(FILE *fin,FILE *fout)
{
  int i,j = 0,k=0;
  i = fgetc(fin);
  if (i != -1){
    //printf("'%c': %s\n", i, code[i]);
      while(j == 1 || j == 0){
	
	
	j = code[i][k] - 48;
	if (j == 1|| j == 0){
	  //printf("%d\n",j);
	  Write_Bit(j,fout);
	}
	k++;
      }
  }
  else {
    //printf("EOF\n");
    while(j == 1||j == 0){
      j = code[11][k] - 48;
	if (j == 1|| j == 0){
	  //printf("bit doe");
	  Write_Bit(j,fout);
	}
	k++;
    }
  }
  return i;
}
//Removes from q
fnode *qremove(void)
{
	int i, l;
	fnode *n = q[i = 1];
 
	if (qend < 2) return 0;
	qend--;
	while ((l = i * 2) < qend) {
		if (l + 1 < qend && q[l + 1]->freq < q[l]->freq) l++;
		q[i] = q[l], i = l;
	}
	q[i] = q[qend];
	return n;
}

//Function to Build the code indexes for the
//different characters values. It recurses 
//until it finds a leaf nodes and as it does 
//this it is building the sequence based off
//of which child is visited. 1 for rigth and
//0 for left. 
void Build_Code(fnode *n, char *s, int len)
{
  static char *out = buf;
  unsigned char c;
  if (n->c){
    s[len] = 0;
    strcpy(out,s);
    c = n->c;
    code[c] = out;
    out += len+1;
    return;
  }
  s[len] = '0';
  Build_Code(n->left, s, len+1);
  s[len] = '1';
  Build_Code(n->right, s, len+1);
}

//Find_Freq reads through the file and 
//increments a counter value at the index
//of the character value in an array large
//enough for every printable ASCII value.
//The Psuedo EOF character is added to this 
//list with a value of 11. This could have
//been any value between 0 and 32 not 
//including 10 (The linefeed char)
void Find_Freq(FILE *fin)
{
  int i,j, freq[128] = {0},eof = 11;
  char c[16];
  rewind(fin);
  while((i = fgetc(fin))!= -1){
    freq[i]++;
    num_char++;
  }
  
  for (j = 0; j < 128; j++){
    if(freq[j]){
      qinsert(New_Node(freq[j],j,0,0));
    }
  }
  qinsert(New_Node(1,eof,0,0));
  while (qend > 2){
    qinsert(New_Node(0,0,qremove(),qremove()));
  }

  Build_Code(q[1],c,0);
 
  
  rewind(fin);
}

//This function does a preorder traversal
//of the huffman tree that was created and
//outputs it to the file header. This will
//then be deceiphered in the unhuff program
//Bits are sent to Write_Bit() to be processed
void Traverse(fnode *n,FILE *fout)
{
  int bit,i;
  if (n == NULL){
      return;
    }
  Traverse(n->left,fout);
  Traverse(n->right,fout);
  if(n->left == NULL && n->right == NULL){
    Write_Bit(1,fout);
    for (i = 0; i < 8; i++){
      bit = !!((n->c << i) & 0x80);
      Write_Bit(bit,fout);
    }
  }
  else{
    Write_Bit(0,fout);
  }
}

//Inserts into q
void qinsert(fnode *n)
{
  int j, i = qend++;
  while ((j = i / 2)){
    if(q[j]->freq <= n ->freq) 
      break;
    q[i] = q[j];
    i = j;
  }
  q[i] = n;
}

//This function accumulates bits into an 8
//bit wide buffer and once full will write
//to the output file. This is necessary because 
//only have the ability to write by byte.
void Write_Bit(int bit,FILE *fout)
{
  buffer <<= 1;
  if (bit)
    buffer |= 0x1;;

  curBit++;
  if(curBit == 8){
    fwrite (&buffer, 1,1,fout);
    curBit = 0;
    buffer = 0;
  }

}

//Fills the rest of the buffer with 0's
//so data less than 8 bits can be written.
void Flush_Bits(FILE *fout)
{
  while (curBit)
    Write_Bit(0,fout);
}

