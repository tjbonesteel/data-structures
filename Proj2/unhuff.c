#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "unhuff.h"
#define ASCII_COUNT 256
#define End 11


int num_char = 0;

int main(int argc, char **argv)
{
  char *inFilename,*outFilename;
  FILE *fin, *fout;
  tnode *huffman;
  int flag;
  //Check for two args
  if (argc != 2){
    fprintf(stderr,"Incorrect number of input arguments!\n");
    return EXIT_FAILURE;
  }


  //Open compressed input file
  inFilename = argv[1];

  fin = fopen(inFilename,"rb");
  if (fin == NULL){
    fprintf(stderr,"Input file could not be opened!\n");
    return EXIT_FAILURE;
  }

  //Start to Decompress file
  //Build huffman tree based off of header bits
  huffman = Build_Huffman_Tree(fin);


  //Open output file for writing
  outFilename = strcat(inFilename,".unhuff");
  fout = fopen(outFilename, "w");
  if(fout == NULL){
    fprintf(stderr, "Ouput file could not be opened!\n");
    return EXIT_FAILURE;
  }
  
  //Decode the tree with bitstream
  flag = Huffman_Decoding(huffman, fin, fout);
  if (flag != 1){
    fprintf(stderr, "Could not decode the tree!\n");
    return EXIT_FAILURE;
  }


  //Close files and free allocated memory
  fclose(fin);
  fclose(fout);
  return EXIT_SUCCESS;
}

tnode *Build_Huffman_Tree(FILE *fin)
{
  // initialize the stack;
  static int pos = -1;
  static int read_char = 0;
  lnode stack;
  stack.next = NULL;
  stack.tree = (tnode *)0;

  int token;//what will be read from file

  while(1) {
    //get a bit here
    //token = fgetc(fin);
    if (pos < 0) {
      read_char = fgetc(fin);
      if (read_char == End){
	token = -1;
	break;
      }
      pos = 7;
    }
    int mask = 1 << pos;
    pos -= 1;
    //changed - to =
    token = read_char & mask;
    if ((read_char & mask) == 0){
	  token = '0';
      }else{
	  token = '1';
      }


    if(token == '0'){
      // pop, pop, push
      if ((Stack_Size(&stack)) <= 1){
	  break;
	}
      
      lnode *right_node = Pop(&stack);
      lnode *left_node = Pop(&stack);
      tnode *right_tree = right_node->tree;
      tnode *left_tree = left_node->tree;
	
      tnode *tree_node = Tree_Construct(ASCII_COUNT,left_tree, right_tree);
	
      if(tree_node == NULL){
	Node_Destruct(left_node);
	Node_Destruct(right_node);
	token = -1;
	break;
      }

      //reuse one of the two popped nodes
      right_node->tree = tree_node;
      right_node->next = NULL;
      Push(&stack,right_node);
      free(left_node);
    }
    else if(token == '1'){
      
      //read a character, build a singleton tree, push
      //read a character, possibly
      //token = fgetc(fin)

      //pos still remains invalid
      if (pos < 0){
	token  = fgetc(fin);
	if (token == End){
	  break;
	}
      }else{
	// have to form a token from two bytes
	// first part is from read_char, then have todo a fgetc to get
	// a new read_char to get the remaining part
	// At the end Pos stays the same
	// the # of valid bits is Pos+1
	//
	// Forming the fixt 6 bits
	// 1. Form mask of form 00111111
	//    Must reside in least significant bits
	//    Take 8 1's and right shift by amount of valid bits
	mask = 0xFF >> (7 - pos);
	token = (read_char & mask) << (7 - pos);
	// 2. get a new char
	read_char = fgetc(fin);
	if (read_char == End){
	  //Must have run out of bytes/bits
	  token = EOF;
	  break;
	}
	token = token | (read_char >> (pos+1));
      }
   
      //read a character, build a singleton tree, push
      
     
      tnode *tree_node =  Tree_Construct(token,NULL,NULL);
      if (tree_node == NULL){
	token = -1;
	break;
      }
      lnode *stack_node = Node_Construct(tree_node);
      if (stack_node == NULL){
	Tree_Destruct(tree_node);
	token = -1;
	break;
      }
      Push(&stack, stack_node);
    }
    else {
      break;
    }
  
  }

  

  if ((token == '0') && (Stack_Size(&stack) == 1)){
    lnode *node = Pop(&stack);
    tnode *huffman = node->tree;
    free(node);
    return huffman;
    

  }
  //Dind't build tree correctly
  Stack_Flush(&stack);
  return NULL;
  
}

//Push operation for lnode stack
void Push(lnode *head, lnode *node)
{
  node -> next = head->next;
  head->next = node;
  int size = (unsigned long)(head->tree)+1;
  head->tree = (tnode*)((unsigned long)size); 
}

//Pops and returns head of lnode stack
lnode *Pop(lnode *head)
{
  lnode * node = head->next;
 
    if(node != NULL) {
      head->next = node->next;
      node->next  = NULL;
      int size = (unsigned long)(head->tree) - 1;
      head->tree = (tnode *)((unsigned long)size);
    }
    return node;
}

//Returns the size of lnode stack
int Stack_Size(lnode *head)
{
  int size = (unsigned long)(head->tree);
  return size;
}

//This function begins the decoding process. It uses the number
//of characters to control the number of calls to the recursive
//Decode_Char helper function. Once the Pseudo EOF character is
//reached the function will return success deconted by a 1.
int Huffman_Decoding(tnode *huffman, FILE *fin, FILE *fout)
{
  int char_count;
  if (fread(&char_count, sizeof(int), 1, fin) !=1 ){
    return 0;
    }
  while(char_count){
    int decoded_char = Decode_Char(huffman, fin);
    if(decoded_char != -1){
      //Pseudo EOF Character
      if(decoded_char != 11)
	fputc(decoded_char, fout);
      char_count--;
    }
    else{
      return 0;
    }
  }
  if (char_count != 0)
    return 0;
  else
    return 1;
}

//This function decodes the bitstream and returns a character
//value contained in the Huffman tree. The file is read by
//bytes and then masked off into individual bits. Each time
//a new node is reached it is tested if it is a leaf node. 
//If so, the function returns its value.
int Decode_Char(tnode *tree, FILE *fin)
{
  static int read_char = 0;
  static int pos = -1;
  int token, mask;
  if(tree==NULL)
    return -1;
  if(Is_Leaf_Node(tree))
    return tree->value;

  if(pos < 0){
    read_char = fgetc(fin);
    if(read_char == EOF){
      token = -1;
    }
    pos = 7;
  }
  mask = 1 << pos;
  pos -= 1;
  token = read_char & mask;
  if ((read_char & mask) == 0){
    token = 0;
  }
  else{
    token = 1;
  }
  if (token == 0)
    return Decode_Char(tree->left, fin);
  else
    return Decode_Char(tree->right, fin);

  return -1;
}

//Tree_Construct creates an instance of the tnode data
//structure with the values as parameters to the function

tnode *Tree_Construct(int key, tnode *left, tnode *right)
{
  tnode *node = (tnode *)malloc(sizeof(tnode));
  
  if (node == NULL){
    return NULL;
  }
  node->value = key;
  node->left = left;
  node->right = right;
  return node;

}


//Simple tree destruction
void Tree_Destruct(tnode *node)
{
  if (node == NULL){
    return;
  }
  Tree_Destruct(node->left);
  Tree_Destruct(node->right);
  free(node);
}

//Checks if the node has a valid ASCII value. Only true
//if the node is a leaf node.
int Is_Leaf_Node(tnode *node)
{
  
  if(node->value > ASCII_COUNT-1 || node->value < 10){
    return 0;
  }else
    return node->value;
}

//lnode Constructor
lnode *Node_Construct(tnode *tree){
  lnode * node = (lnode *)malloc(sizeof(lnode));
  if (node == NULL){
    return NULL;
  }
  node->tree = tree;
  node->next = NULL;
  return node;
}

//lnode destructor
void Node_Destruct(lnode * node)
{
  if (node != NULL){
    Tree_Destruct(node->tree);
    free(node);
  }
  
}
//Pretty straight forward stack flush operation
void Stack_Flush(lnode *node)
{
  lnode *curr = node->next;
  while (curr != NULL){
    lnode *tmp = curr->next;
    Node_Destruct(curr);
    curr = tmp;

  }
  node->next = NULL;
  node->tree  = (tnode *)0;
}
