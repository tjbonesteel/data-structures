#include <stdio.h>
#include <string.h>

typedef struct _fnode{
  int freq;
  char c;
  struct _fnode *left,*right;
}fnode;

void Traverse(fnode *n,FILE *fout);
void Build_Code(fnode *n, char *c, int len);
void qinsert(fnode *n);
void Find_Freq(FILE *fin);
fnode *New_Node(int freq, char c, fnode *a, fnode *b);
void Write_Bit(int bit,FILE *fout);
void Flush_Bits(FILE *fout);
int Generate_Bitstream(FILE *fin, FILE *fout);
