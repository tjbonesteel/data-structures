#include <stdio.h>
#include <string.h>


typedef struct _tnode{
  int value;
  struct _tnode *left;
  struct _tnode *right;
}tnode;

typedef struct _lnode{
  tnode *tree;
  struct _lnode *next;
}lnode;


tnode *Build_Huffman_Tree(FILE *Fin);
void Push(lnode *head, lnode *lnode);
lnode *Pop(lnode *head);
int Stack_Size(lnode *head);
tnode *Tree_Construct(int key, tnode *left, tnode *right);
void Tree_Destruct(tnode *tnode);
int Is_Leaf_Node(tnode *node);
lnode *Node_Construct(tnode *tree);
void Node_Destruct(lnode *node);
void Stack_Flush(lnode *node);
int Huffman_Decoding(tnode *huffman, FILE *fin, FILE *fout);
int Decode_Char(tnode *tree, FILE *fin);
