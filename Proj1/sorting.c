//Program written by Trevor Bonesteel
//For ECE368 Project 1

#include <stdlib.h>
#include <math.h>
#include <stdio.h>
#include "sorting.h"

void swap(long *a, long *b);

//Similar to Save_seq1
//Save to array used by sorting algorithm
int *Gen_Seq1(int Size,int *n){


  int *gaps, *gTemp = NULL;
  int size = 1;
  
  gaps = malloc(sizeof(int));
  

  gaps[0] = 1;

  int p2 = 0,p3 = 0,i,u2,u3;
  
  for (i = 1; gaps[i-1] < Size; i++){
    if (gaps[p2] * 2 == gaps[i - 1])
      p2 += 1;
    if (gaps[p3] * 3 == gaps[i - 1])
      p3 += 1;
    u2 = gaps[p2] * 2;
    u3 = gaps[p3] * 3;
    
    if (u2 < u3){
      p2 += 1;
      gaps[i] = u2;
    }
    else{
      p3 += 1;
      gaps[i] = u3;
    }
    if (i == size){
      size = size * 2;
      gTemp = realloc(gaps, sizeof(int) * size);
      if (gTemp == NULL){
	free(gaps);
	return NULL;
      }
      else{
	gaps = gTemp;
      }
    }
  }
    free(gTemp);
    *n = i - 2;
    return gaps;
}


long *Load_File(char *Filename, int *Size)
{
  //local variable declaration
  int n,i=0;
  long l;
  long *Array;
  
  //Preparing input file for read
  FILE *fptr = fopen(Filename,"r");
  if (fptr == NULL){
    return NULL;
  }

  //Reading in number of elements in array
  fscanf(fptr,"%d",&n);
  *Size = n;

  //Allocate necessary space for array
  Array = malloc(sizeof(long) * n);

  //Read in values of array
  while( !feof(fptr)){
    fscanf(fptr,"%ld",&l);
    Array[i] = l;
    i++;

  }
  //Close input file

  fclose(fptr);

  
  //Return array read from file
  return Array;
}

int Save_File(char *Filename, long *Array, int Size)
{
  //Local variables
  int i;

  //Open file for writing
  FILE *fptr = fopen(Filename, "w");
  if (fptr == NULL){
    return -1;
  }
  
  //Print number of Values
  fprintf(fptr,"%d\n",Size);

  //Loop through array and print to file
  for (i = 0; i < Size; i++){
    fprintf(fptr,"%ld\n",Array[i]);
  }

  //close output file
  fclose(fptr);

  //return number of values saved to file
  return i;
  
  //return 0;
}

//Simple swap utilizing pointers to Longs
void swap(long *a, long *b)
{
  long temp;
  temp = *a;
  *a = *b;
  *b = temp;
}

void Shell_Insertion_Sort(long *Array, int Size, double *N_Comp, double *N_Move)
{
  int *gaps,n;
  int curGap;
  int i,j;
  gaps = Gen_Seq1(Size,&n);
  
  for (i = n;i >= 0;i--){
    curGap = gaps[i];
    for (j = Size-1; j-curGap >= 0; j--){
      *N_Comp += 1;
      if (Array[j-curGap] > Array[j]){
	swap(&Array[j-curGap],&Array[j]);
	*N_Move += 3;
      }
    }
  }
}

void Improved_Bubble_Sort(long *Array, int Size, double *N_Comp, double *N_Move)
{
  
  long temp = 1;
  int j,swapped = 1;
  int N1 = 0;
  int N = Size;
  temp = (double)N/1.3;
  while (temp > 1 || swapped == 1){
    N1 = floor(temp);
    if (N != N1){
      N = N1;
      if (N == 9 || N == 10){
	N = 11;
      }
      if (N < 1){
	N = 1;
      }
      swapped = 0;
	for ( j = 0; j+N <= Size-1; ++j){
	  *N_Comp += 1;
	  if (Array[j] > Array[j+N]){
	    *N_Move += 3;
	    swap(&Array[j], &Array[j+N]);
	    swapped = 1;
	  }
	}
      }
    
    temp = (double)N/1.3;
  }
   
}

//Generates Seq2 and saves to file
void Save_Seq2 (char *Filename, int N)
{
  int count = 1;
  double temp = 1;
  int N1 = 0;
  FILE *fptr = fopen(Filename, "w");
  if (fptr == NULL){
    return;
    }
  fprintf(fptr,"%d\n",N);
  temp = (double)N/1.3;
  while (temp >= 1){
    N1 = floor(temp);
    if (N != N1){
      N = N1;
      if (N == 9 || N == 10){
	N = 11;
      }
      fprintf(fptr,"%d\n",N);
    }
    temp = (double)N/(1.3);
    count++;
  }
  fclose(fptr);

}

//Generates Seq1 and saves to file
//3 Smooth numbers
void Save_Seq1 (char *Filename, int N)
{
  int *gaps, *gTemp = NULL;
  int size = 1;
  FILE *fptr = fopen(Filename, "w");
  if (fptr == NULL){
    return;
  }

  gaps = malloc(sizeof(int));
  

  gaps[0] = 1;

  int p2 = 0,p3 = 0,i,u2,u3;
  
  for (i = 1; gaps[i-1] < N; i++){
    if (gaps[p2] * 2 == gaps[i - 1])
      p2 += 1;
    if (gaps[p3] * 3 == gaps[i - 1])
      p3 += 1;
    u2 = gaps[p2] * 2;
    u3 = gaps[p3] * 3;
    
    if (u2 < u3){
      p2 += 1;
      gaps[i] = u2;
    }
    else{
      p3 += 1;
      gaps[i] = u3;
    }
    
    if (i == size){
      size = size * 2;
      gTemp = realloc(gaps, sizeof(int) * size);
      if (gTemp == NULL){
	free(gaps);
	return;
      }
      else{
	gaps = gTemp;
      }
    }
  }
  //Reverses the sequence as it prints to file
    free(gTemp);
    i -= 2;
    fprintf(fptr,"%d\n",N);
    for (;i >= 0;i--){
      fprintf(fptr,"%d\n",gaps[i]);

    }
    free(gaps);
    fclose(fptr);


}

